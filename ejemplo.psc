// Calcula el promedio de una lista de N datos utilizando un SubProceso

Funcion prom <- Promedio ( arreglo, cantidad )
	suma <- 0
	Para i<-0 Hasta cantidad-1 Hacer
		suma <- suma + arreglo[i]
	FinPara
	prom <- suma/cantidad
FinFuncion

Algoritmo Principal

	Dimension datos[100]
	Escribir "Ingrese la cantidad de datos:"
	Leer n
	
	Para i<-0 Hasta n-1 Hacer
		Escribir "Ingrese el dato ",i+1,":"
		Leer datos[i]
	FinPara
	
	Escribir "El promedio es: ",Promedio(datos,n)
	
FinAlgoritmo
